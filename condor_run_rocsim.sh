#!/bin/bash

tar -xzf python_sci.tar.gz
export PATH=$(pwd)/python/bin:$PATH

python rocsim.py --unattended

rm -Rf python_sci.tar.gz
rm -Rf python
rm rocsim.py
