import matplotlib.pyplot as plt
import numpy as np


# ------------------------------------------------
# Library of functions

def fit_binding_from_steps(steps, binding=0.882351):
    """
    Linear least-squares fit the binding value from the number of steps.

    :param steps: int, the number of steps
    :param binding: float, the binding value to match
    :return: float, the binding value to use
    """

    from scipy.optimize import least_squares
        
    def fit_geometric_rate(s, b):
        out = b
        for i in range(s - 1):
            out += (1 - out)*b
        return out

    fit = lambda v: np.array([fit_geometric_rate(steps, v[0]) - binding])
    
    return least_squares(fit, 0.3).x[0]


def n_filled_oors(norigins, turnover_slope, 
                  turnover_offset, duplication_prob):
    """
    Calculate the number of origins of replication that can be filled
    based on the current number of origins of replication that exist in
    a vector of cells.

    :param norigins: vector of ints, the number of origins of 
    replication in each cell
    :param turnover_slope: float, the slope of a sigmoid that describes 
    replication mac`hinery turnover
    :param turnover_offset: float, the offfset of a sigmoid that 
    describes replication machinery turnover
    :param duplication_prob: float, the probability that any one
    origin of replication will be bound by an available polymerase. This
    is designed to describe the fact that one never finds 100% of
    plasmids replicated, even in the case of 5 plasmids in a cell.
    :return: vector, number of origins that can be filled
    """

    # Second, account for turnover (destruction) of polymerases using a sigmoid
    filled = 1.0/(1.0 + np.exp(turnover_slope*(norigins - turnover_offset)))
    filled = (filled*norigins).astype(np.int32)
    filled = np.clip(np.random.binomial(filled, duplication_prob), 0, filled)

    return filled


def lytic_cycle_onsets(ncells, replication_offset=3, rederive=False):
    """
    Create a vector of times at which cells will begin the lytic 
    cycle, based on observations.  # TESTED
    
    :param ncells: The number of ncells to simulate
    :param replication_offset: float, the number of rounds of 
    replication to offset the determined entry of each cell into the
    lytic cycle, multipled by the length of one round of replication.
    This compensates for the fact that lytic entry can only be measured
    above a threshold number of plasmids in the fit biological data.
    :param rederive: If true, rederive the constants for fitting the
        fraction of cells that have entered the lytic cycle
    """

    if rederive:
        from scipy.optimize import curve_fit
        
        def fit_sigmoid(x, a, b, c):
            return c/(1.0 + np.exp(-a*(x - b)))

        # Real data
        hours = np.array([0., 9., 9.5, 11., 13., 17., 19., 21., 25.])
        fraction_lytic = np.array([0, 0.105, 0.131, 0.146, 0.25, 0.285, 0.43, 0.565, 0.615])

        hours -= replication_offset
        hours[hours < 0] = 0

        popt, pcov = curve_fit(fit_sigmoid, hours, fraction_lytic,
                               bounds=([0.05, 0, 0.615], [0.5, 40, 1.0]))
        k, x0, peak = popt
        x0 += replication_offset
    else:
        k = 0.2079
        x0 = 17.83
        peak = 0.7744

    # Adjust the sigmoid such that it represents the 
    x0 -= min(replication_offset, 4)

    # Make it so that a fraction of cells do not enter the lytic cycle
    nnans = int(round((1.0 - peak)*ncells))
    nanpart = np.zeros(nnans)
    nanpart[:] = 999999

    # Determine the timing for the remaining cells
    x = np.linspace(0, 100, 10000)
    pdf = k*(np.exp(-k*(x - x0)))/((1 + np.exp(-k*(x - x0)))*(1 + np.exp(-k*(x - x0))))
    pdf /= np.sum(pdf)
    times = np.sort(np.random.choice(x, ncells - nnans, p=pdf))

    # The onset times of the lytic cycle of each cell
    return np.concatenate([times, nanpart])


def initial_plasmids_per_cell(ncells, mu, sigma):
    """
    Determine the initial distributions of plasmids per cell.

    :param ncells: int, number of cells to simulate
    :param mu: float, the mean number of plasmids per cell
    :param sigma: float, the standard deviation of plasmids per cell

    :return: vector of number of plasmids per cell
    """

    rands = np.random.normal(mu, sigma, ncells)
    return np.clip(np.round(rands).astype(np.int32), 0, None)


class CellPlasmids():
    """
    A class that contains all of the plasmids of a single type in a
    vector of cells. Key is that it allows plasmids to complete 
    replication at multiple times, efficiently.
    """

    def __init__(self, plasmid_distribution, steps, dt, oors):
        """
        Create a set of cells based on an initial distribution.

        :param plasmid_distribution: vector of ints, the number of
        plasmids per cell.
        :param steps: int, the number of time steps during replication
        :param dt: float, the time step size
        :param oors: int, the number of origins of replication
        """

        self.ncells = len(plasmid_distribution)
        self.noors = oors
        self.nsteps = steps
        self.pos = 0  # The time position at the next step

        self.dt = dt
        self.ts = np.arange(1, steps+1)*dt
        self.non_replicated = plasmid_distribution
        self.active = []
        for o in range(oors):
            self.active.append(np.zeros((steps, self.ncells), dtype=np.int32))

    def step(self):
        """Step forward in time. Must be called each step."""

        # Update the time vector
        current = self.pos
        self.pos = (self.pos + 1)%self.nsteps

        # Clear any completed plasmids
        for o in range(self.noors):
            # print self.noors, self.ts[current], np.sum(self.active[o][current, :])
            self.non_replicated += 2*self.active[o][current, :]
            self.active[o][current, :] = 0

        self.ts[current] += self.dt*self.nsteps
        # REMEMBER: self.pos now marks the position of the next time
        return self

    def open_oors(self, pos):
        """
        Return the number of open origins of replication per cell.

        :param pos: vector of positions of cells in the lytic cycle
        
        :return: vector of ints, the number of open oors per cell
        """

        out = self.non_replicated[pos]*self.noors
        for o in range(self.noors - 1):
            out += (self.noors - (o + 1))*np.sum(self.active[o][:, pos], axis=0)

        return out

    def total_plasmids(self):
        """Return the number of plasmids per cell."""

        out = np.copy(self.non_replicated)
        for o in range(self.noors):
            for t in range(self.nsteps):
                out += self.active[o][t, :]
        return out

    def add_polymerases(self, pos, add, reptime):
        """
        Add polymerases to plasmids in each of the cells, correctly
        accounting for the change in duplication rate.

        :param pos: vector of positions of cells in the lytic cycle
        :param add: vector of ints, the number of polymerases to add
        :param reptime: float, the amount of time it will take to 
        replicate a single plasmid
        to each cell
        """

        # print self.noors, add, reptime

        if self.noors == 1:
            # Treat this (e.g. 4012) separately because we
            # can avoid random number generation
            add_int = np.rint(add).astype(np.int32)
            add_int = np.minimum(self.non_replicated[pos], add_int)
            self.non_replicated[pos] -= add_int
            step_forward = int(self.pos - 2 + -(-reptime//self.dt))%self.nsteps
            self.active[0][step_forward, pos] += add_int
        
        elif self.noors == 2:
            time_now = self.ts[self.pos] - self.dt
            
            # We are forced to iterate over cells because the 
            # distribution across times is different per cell.
            for cell in np.where(pos)[0]:
                group = [2*int(self.non_replicated[cell])]
                for o in range(self.noors - 1):
                    group.extend(self.active[o][:, cell])

                if np.sum(group) > 0:
                    # Separate out the number of origins bound with two open
                    # origins of replication versus one open at various
                    # stages of completion.
                    adds = np.random.multinomial(add[cell], group/np.sum(group).astype(np.float64))

                    # Calculate the number of origins bound (accounting for 
                    # two per plasmid or one per plasmid) using binomial
                    # distribution equation (but not drawn from binomial)
                    if self.non_replicated[cell] > 0:
                        p = float(adds[0])/self.non_replicated[cell]
                        q = 1.0 - p
                        _, singles, doubles = np.random.multinomial(adds[0], [q*q, 2*p*q, p*p])

                        # Catch a weird error that shouldn't crop up but is possible
                        if singles + doubles > self.non_replicated[cell]:
                            doubles = self.non_replicated[cell] - singles
                            if doubles < 0:
                                doubles = 0
                                singles = self.non_replicated[cell]

                    # Catch a weird error
                    else:
                        doubles = 0
                        singles = min(add[cell], self.non_replicated[cell])

                    # Add singles
                    step_forward = max(int(-(-reptime//self.dt)), 1)
                    step_forward = (self.pos - 2 + step_forward)%self.nsteps
                    self.active[0][step_forward, cell] += singles

                    # Add doubles into place, accounting for too-big steps
                    step_forward = max(int(-(-reptime/2.0//self.dt)), 1)
                    step_forward = (self.pos - 2 + step_forward)%self.nsteps
                    self.active[-1][step_forward, cell] += doubles

                    # And add the remainder of singles, correcting the times
                    # in each case.
                    for t in range(self.nsteps):
                        repremain = self.ts[t] - time_now
                        step_forward = max(int(-(-repremain/2.0//self.dt)), 1)
                        step_forward = (self.pos - 2 + step_forward)%self.nsteps
                        adds[t+1] = min(adds[t+1], self.active[0][t, cell])
                        self.active[0][t, cell] -= adds[t+1]
                        self.active[1][step_forward, cell] += adds[t+1]
        else:
            raise NotImplementedError('Have not yet implemented the code to compute more than 2 origins of replication.')


class LyticEntryPopulation():
    def __init__(self, ps, po, r1, ofr=3, time_steps=6, binding_rate=0.882351, 
                 t_4012_relative_p3hr1=0.25, mu_p3hr1=25, mu_4012=5):
        self.polymerase_turnover_slope = ps
        self.polymerase_turnover_offset = po
        self.replication_time_160kb = r1
        self.offset_rounds = ofr
        self.dt = r1/time_steps
        self.steps = time_steps
        self.p4012_replication_time = t_4012_relative_p3hr1*r1
        
        self.binding_rate = fit_binding_from_steps(time_steps, binding_rate)
        self.mu_p3hr1 = mu_p3hr1
        self.sigma_p3hr1 = mu_p3hr1/5.0
        self.mu_4012 = mu_4012
        self.sigma_4012 = mu_4012/5.0

        self.p3hr1 = None
        self.p4012 = None

        self._count_ratios = [[], []]
        self._filled_percentage = [[], []]

    def simulate(self, ncells=1000, hours=60, plasmids_per_cell=False):
        """
        Run a simulation.

        :param hours: int, the number of hours to run for
        """

        # Initial parameters
        p3hr1 = CellPlasmids(
                    initial_plasmids_per_cell(ncells, self.mu_p3hr1, self.sigma_p3hr1),
                    self.steps, self.dt, oors=2)
        p4012 = CellPlasmids(
                    initial_plasmids_per_cell(ncells, self.mu_4012, self.sigma_4012),
                    self.steps, self.dt, oors=1)

        if plasmids_per_cell:
            lytic_entry = np.zeros(ncells)
        else:
            lytic_entry = lytic_cycle_onsets(ncells, self.offset_rounds*self.replication_time_160kb)

        # Output
        sim_hours = [0]
        sim_plasmids_p3hr1 = [0]
        sim_plasmids_p4012 = [0]

        for h in np.arange(self.dt, hours + self.dt/2, self.dt):
            # Step each plasmid type
            p3hr1.step()
            p4012.step()

            # Determine cells that have entered lytic phase
            pos = (h >= lytic_entry)
            oors_p3hr1 = p3hr1.open_oors(pos)
            oors_p4012 = p4012.open_oors(pos)

            groups = np.array([oors_p3hr1, oors_p4012])
            total_open = np.sum(groups, axis=0)

            if np.sum(total_open) > 0:
                # Calculate the number to fill
                add = n_filled_oors(total_open, self.polymerase_turnover_slope, 
                                    self.polymerase_turnover_offset, self.binding_rate)

                # Then, use a multinomial distribution to divvy 
                # up polymerases between groups
                groups = groups.astype(np.float32)/total_open
                n_p3hr1s, n_4012s = np.zeros(np.sum(pos)), np.zeros(np.sum(pos))
                for i, cell in enumerate(np.where(pos)[0]):
                    n_p3hr1s[i], n_4012s[i] = np.random.multinomial(add[cell], groups[:, cell])

                p3hr1.add_polymerases(pos, n_p3hr1s, self.replication_time_160kb)
                p4012.add_polymerases(pos, n_4012s, self.p4012_replication_time)

            sim_hours.append(h)
            sim_plasmids_p3hr1.append(np.mean(p3hr1.total_plasmids()))
            sim_plasmids_p4012.append(np.mean(p4012.total_plasmids()))

            if plasmids_per_cell:
                self._count_ratios[0].append(p3hr1.total_plasmids())
                self._count_ratios[1].append(p4012.total_plasmids())

                self._filled_percentage[0].append(p3hr1.open_oors(pos))
                self._filled_percentage[1].append(p4012.open_oors(pos))

            if np.max(p3hr1.total_plasmids()) > 50000 or np.max(p4012.total_plasmids()) > 50000:
                return False, False, False

        self.p3hr1 = p3hr1
        self.p4012 = p4012

        return sim_hours, sim_plasmids_p3hr1, sim_plasmids_p4012

    def final_plasmid_counts(self):
        """
        Return the final number of plasmids of each type per cell.
        """

        return self.p3hr1.total_plasmids(), self.p4012.total_plasmids()

    def count_ratios(self):
        """
        The ratio of p3hr1 counts to 4012 counts for each cell at each time.
        """

        p3hr1_counts = np.array(self._count_ratios[0])
        p4012_counts = np.array(self._count_ratios[1])

        return p3hr1_counts/p4012_counts

    def filled_percentages(self):
        """
        Return the number of filled OORs at any time point.
        """

        p3hr1_counts = np.array(self._count_ratios[0])
        p4012_counts = np.array(self._count_ratios[1])
        
        p3hr1_open = np.array(self._filled_percentage[0])
        p4012_open = np.array(self._filled_percentage[1])

        p3hr1_filled = 1.0 - p3hr1_open.astype(np.float64)/(p3hr1_counts*2)
        p4012_filled = 1.0 - p4012_open.astype(np.float64)/(p4012_counts)

        return p3hr1_filled, p4012_filled


def save_variants(polymerase_turnover_slope=0.0005,
                  polymerase_turnover_offset=15500,
                  replication_1_time=1.95,
                  lytic_offsets=0, 
                  binding_rate=0.90,
                  time_steps_by_6=4,
                  mu_p3hr1=25,
                  mu_4012=13,
                  size_4012_vs_p3hr1=0.25,
                  additional_plots=False):
    """
    Search over variable space to find the optimal parameters
    """

    real_hours = np.array([0, 9, 11, 20, 33, 36, 40, 60])
    real_plasmids_per_cell = np.array([25.4, 20.3, 121.7, 2593., 7249., 7793., 7968., 8398.])
    real_4012_hours = np.array([0, 11, 20, 33, 36, 40, 60])
    real_4012_per_cell = np.array([4.5, 46.6, 1520.8, 4611.3, 3979.5, 3177.6, 4183.6])

    ps = polymerase_turnover_slope
    po = polymerase_turnover_offset
    r1 = replication_1_time
    offs = lytic_offsets
    br = binding_rate
    time_steps = 6*time_steps_by_6
    ts = time_steps

    title = 'po %i ps %.4f r1 %.2f br %.2f st %i of %i p3 %.0f 40 %.0f sz %.2f' % (
        po, ps, r1, br, ts, offs, mu_p3hr1, mu_4012, size_4012_vs_p3hr1)

    lep = LyticEntryPopulation(ps, po, r1, offs, binding_rate=br, time_steps=ts, mu_p3hr1=mu_p3hr1, mu_4012=mu_4012, t_4012_relative_p3hr1=size_4012_vs_p3hr1)
    hrs, p3hr1, p4012 = lep.simulate()
    
    if hrs == False or p3hr1 == False:
        print('Simulation failed.')
        exit(1)

    plt.plot(hrs, p4012, c='#47AEED')
    plt.plot(hrs, p3hr1, c='#D61E21')
    plt.scatter(real_4012_hours, real_4012_per_cell, c='#47AEED')
    plt.scatter(real_hours, real_plasmids_per_cell, c='#D61E21')
    plt.xlabel('TIME (hours)')
    plt.ylabel('POPULATION PLASMIDS/CELL')
    plt.yscale('symlog')
    plt.title('Average across population, matches real data')
    plt.savefig(title + '-pop.pdf')
    plt.clf()

    if additional_plots:
        lep = LyticEntryPopulation(ps, po, r1, offs, binding_rate=br, time_steps=ts, mu_p3hr1=mu_p3hr1, mu_4012=mu_4012, t_4012_relative_p3hr1=size_4012_vs_p3hr1)
        hrs, p3hr1, p4012 = lep.simulate(hours=30, plasmids_per_cell=True)
        
        plt.plot(hrs, p4012, c='#47AEED')
        plt.plot(hrs, p3hr1, c='#D61E21')
        plt.xlabel('TIME (hours)')
        plt.ylabel('PER CELL PLASMIDS/CELL')
        plt.title('Average plasmids per cell after lytic onset')
        plt.savefig(title + '-percell.pdf')
        plt.clf()

        # np3hr1, n4012 = lep.final_plasmid_counts()
        # plt.scatter(np3hr1, n4012)
        # plt.xlabel('N 4012/CELL')
        # plt.ylabel('N p3HR1/CELL')
        # plt.title('Individual plasmids per cell')
        # plt.savefig(title + '-percell-scatter.png')
        # plt.clf()

        rat = lep.count_ratios()
        for cell in range(np.shape(rat)[1]):
            plt.plot(hrs[1:], rat[:, cell], lw=0.5, c='#CCCCCC')
        plt.plot(hrs[1:], np.mean(rat, axis=1), lw=2, c='#000000')
        plt.xlabel('TIME (hours)')
        plt.ylabel('RATIO p3HR1/4012')
        plt.ylim([0, 30])
        plt.title('Ratio of p3hr1 to 4012 per cell')
        plt.savefig(title + '-ratio.pdf')
        plt.clf()

        frac_p3hr1, frac_4012 = lep.filled_percentages()
        for cell in range(np.shape(rat)[1]):
            plt.plot(hrs[1:], frac_4012[:, cell], lw=0.5, c='#47AEED')
            plt.plot(hrs[1:], frac_p3hr1[:, cell], lw=0.5, c='#D61E21')
        plt.plot(hrs[1:], np.mean(frac_4012, axis=1), lw=2, c='#47AEED')
        plt.plot(hrs[1:], np.mean(frac_p3hr1, axis=1), lw=2, c='#D61E21')
        plt.xlabel('TIME (hours)')
        plt.ylabel('FILLED OORS')
        plt.title('Fraction of filled OORs by time')
        plt.savefig(title + '-filled.pdf')
        plt.clf()

        x = np.arange(0, 50000)
        y = 1.0/(1.0 + np.exp(ps*(x - po)))
        y = y*0.9

        plt.plot(x, y)
        plt.xlabel('ORIGINS OF REP IN CELL')
        plt.ylabel('ORIGINS OF REP BOUND')
        plt.title('Binding of origins by polymerase per cell')
        plt.savefig(title + '-pol.pdf')
        plt.clf()


if __name__ == '__main__':
    # test_value()
    save_variants()
