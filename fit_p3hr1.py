import numpy as np
from scipy.optimize import curve_fit


def sigmoid(x, x0, k, peak):
    """
    Return a sigmoid given x values, and the fits x0 and k, needed for
    rederiving the fits in inittimes
    :param x: a vector of x values
    :param x0: the offset along the x axis from 0
    :param k: the skew
    :param peak: the maximum fraction of cells, [0, 1]
    :return: a vector of y values that match the x values
    """

    # 10 hrs 15%, 20 hrs 60%, 30 hrs 65% (fraction of cells in lytic cycle)

    y = peak/(1 + np.exp(-k*(x - x0)))
    return y


def init_plasmid_distribution_per_cell(mu, sigma, n):
    """
    Create a vector of cells with a mean, standard deviation, and n
    :param mu: The mean plasmids per cell
    :param sigma: The standard deviation of plasmids per cell
    :param n: The number of cells
    :return: A vector of cells with numbers of plasmids
    """

    cells = np.random.normal(mu, sigma, n).astype(np.int32)
    cells[cells < 0] = 0
    return cells


def distance_metric(real, test, use_chisq=False):
    """
    We have used a modified chi-square and a real chi-square.
    :param real: a vector of real data, optionally if 2d, estimate the best fits for y
    :param test: a vector of estimated data, optionally 2d
    :return: float distance metric
    """

    if real.ndim > 1 and test.ndim > 1:
        test = np.interp(real[0, :], test[0, :], test[1, :], np.nan, np.nan)
        real = real[1, :]
        nonnan = np.bitwise_and(np.isfinite(test), np.isfinite(real))
        test = test[nonnan]
        real = real[nonnan]

    if use_chisq:
        chisq = np.sum(np.square(real - test)/real)
    else:
        chisq = np.sum(np.square(real - test))

    return chisq


def simulate(maximal_synthesis, saturation_constant, polymerase_turnover_slope,
             polymerase_turnover_offset, replication_1_time,
             replication_1_duration, replication_2_time,
             duplication_rate, brdu, measure, dt, initmean, initsd, ncells):
    """
    Run a single simulation with a given pair of theta rate, rolling
        circle rate, and a length of time in the theta phase.
    :param rate1: The rate in the theta phase in terms of hours per duplication
    :param rate2: The rate in the rolling circle phase (hours per duplication)
    :param thetadur: The number of hours in the thetaphase
    :param ratelimit: A limit on the rate of plasmid replication
    :param brdu: The time (in hours) of the brdu pulse
    :param measure: The amount of time to wait until measurement after
        the brdu pulse
    :param dt: step size in time (in hours)
    :param initmean: The mean number of plasmids per cell upon
        initialization
    :param initsd: The standard deviation of plasmids per cell upon init
    :param ncells: The number of cells to initialize
    :return: The fit of the simulation to the data
    """

    ll = init_plasmid_distribution_per_cell(initmean, initsd, ncells)  # Light-light plasmids
    lh = np.zeros(ncells, dtype=np.int32)  # Light-heavy plasmids
    hh = np.zeros(ncells, dtype=np.int32)  # Heavy-heavy plasmids
    ll_dup = np.zeros(ncells, dtype=np.int32)  # Light-heavy plasmids
    lh_dup = np.zeros(ncells, dtype=np.int32)  # Light-heavy plasmids
    hh_dup = np.zeros(ncells, dtype=np.int32)  # Light-heavy plasmids
    lyt = cells_entering_lytic_cycle(ncells)  # The onset times of the lytic cycle of each cell
    ts = np.copy(lyt) - replication_1_time  # The current time of each plasmid for duplication

    plasmids_per_time = [[], []]

    for h in np.arange(dt, brdu+dt/2.0, dt):
        pos = (h >= lyt) & (h > ts)  # (h > ts + replication_time)

        if np.sum(pos) > 0:
            ll[pos] += 2*ll_dup[pos]

            add = nplasmids_to_replicate(ll[pos], maximal_synthesis, saturation_constant,
                                         polymerase_turnover_slope, polymerase_turnover_offset)
            add = np.random.binomial(add, duplication_rate)
            add[add > ll[pos]] = ll[pos][add > ll[pos]]

            ll[pos] -= add
            ll_dup[pos] = add

            pos1 = pos & (h < lyt + replication_1_duration)
            pos2 = pos & (h >= lyt + replication_1_duration)

            ts[pos1] += replication_1_time
            ts[pos2] += replication_2_time

        plasmids_per_time[0].append(h)
        plasmids_per_time[1].append(np.mean(ll + 2*ll_dup))

        if np.max(ll) > 20000:
            raise ValueError('Cells are too big')

    for h in np.arange(brdu, brdu+measure+0.01, dt):
        pos = (h >= lyt) & (h > ts)

        if np.sum(pos) > 0:
            lh[pos] += 2*ll_dup[pos]
            lh[pos] += lh_dup[pos]
            hh[pos] += lh_dup[pos]
            hh[pos] += 2*hh_dup[pos]

            add = nplasmids_to_replicate(ll[pos] + lh[pos] + hh[pos],
                                         maximal_synthesis, saturation_constant,
                                         polymerase_turnover_slope, polymerase_turnover_offset)
            add = np.random.binomial(add, duplication_rate)
            ll_dup[pos], lh_dup[pos], hh_dup[pos] = split_replication_amongst_weights(ll[pos], lh[pos], hh[pos], add)
            ll_dup[pos][ll_dup[pos] > ll[pos]] = ll[pos][ll_dup[pos] > ll[pos]]
            lh_dup[pos][lh_dup[pos] > lh[pos]] = lh[pos][lh_dup[pos] > lh[pos]]
            hh_dup[pos][hh_dup[pos] > hh[pos]] = hh[pos][hh_dup[pos] > hh[pos]]

            ll[pos] -= ll_dup[pos]
            lh[pos] -= lh_dup[pos]
            hh[pos] -= hh_dup[pos]

            pos1 = pos & (h < lyt + replication_1_duration)
            pos2 = pos & (h >= lyt + replication_1_duration)

            ts[pos1] += replication_1_time
            ts[pos2] += replication_2_time

        if np.max(ll) > 20000:
            raise ValueError('Cells are too big')

    # Remove the influence of cells that cannot have replicated
    ts[lyt > h] = np.nan
    ts1 = np.copy(ts)
    ts1[np.invert(h < lyt + replication_1_duration)] = np.nan
    ts2 = np.copy(ts)
    ts2[np.invert(h >= lyt + replication_1_duration)] = np.nan

    return ll, lh, hh, ll_dup, lh_dup, hh_dup, ts1, ts2, plasmids_per_time


def nplasmids_to_replicate(nplasmids, maximal_synthesis, saturation_constant,
                           polymerase_turnover_slope, polymerase_turnover_offset):
    """
    Calculate the number of plasmids to be replicated.

    Saturation constant must be <= maximal synthesis

    :param nplasmids: int, the number of plasmids currently in the cell
    :param maximal_synthesis: float, the maximal rate of plasmid synthesis
    :param saturation_constant: float, the saturation constant
    :return: int, number of plasmids to replicate

    """

    # From Satyagal & Agrawal, Biotechnology and Bioengineering 1989
    # v(p) = (V^0_p * p)/(K_r + p)

    if maximal_synthesis > saturation_constant:
        raise ValueError('Maximal synthesis cannot be larger than saturation constant')

    next_plasmids = (maximal_synthesis*nplasmids)/(saturation_constant + nplasmids)

    # Add turnover sigmoid
    turnover = 1.0/(1.0 + np.exp(polymerase_turnover_slope*(nplasmids - polymerase_turnover_offset)))
    next_plasmids = np.round(next_plasmids*turnover)

    return np.int32(next_plasmids)


def cells_entering_lytic_cycle(n, rederive=False):
    """
    Create a vector of times at which cells will begin the lytic cycle,
    based on observations.
    :param n: The number of times to be calculated
    :param rederive: If true, rederive the constants for fitting the
        fraction of cells that have entered the lytic cycle
    :return: A vector of times (in hours) at which the cycle starts
    """

    # To rederive the constants used to determine the distribution of
    # entry times into the lytic cycle, fit with a sigmoid
    if rederive:
        # Was
        # hours = np.array([10, 20, 30])
        # fraction_lytic = np.array([0.15, 0.60, 0.65])

        def fit_sigmoid(x, a, b, c):
            return c/(1.0 + np.exp(-a*(x - b)))

        hours = np.array([9., 11., 13., 17., 19., 21., 25.])
        fraction_lytic = np.array([0.105, 0.143, 0.25, 0.285, 0.43, 0.565, 0.615])
        popt, pcov = curve_fit(fit_sigmoid, hours, fraction_lytic,
                               bounds=([0.05, 0, 0.615], [0.5, 40, 1.0]))
        k, x0, peak = popt
    else:
        k = 0.206
        x0 = 17.98
        peak = 0.782

    nnans = int(round((1.0 - peak)*n))
    nanpart = np.zeros(nnans)
    nanpart[:] = 999999

    x = np.linspace(0, 100, 10000)
    pdf = k*(np.exp(-k*(x - x0)))/((1 + np.exp(-k*(x - x0)))*(1 + np.exp(-k*(x - x0))))
    pdf /= np.sum(pdf)
    times = np.sort(np.random.choice(x, n - nnans, p=pdf))

    return np.concatenate([times, nanpart])


def split_replication_amongst_weights(ll, lh, hh, dup):
    """
    Split the number of replicated plasmids amongst the three different
    groups based on the relative number of plasmids in each group.

    :param ll: array of ints, the number of light-light plasmids per cell
    :param lh: array of ints, the number of light-heavy plasmids per cell
    :param hh: array of ints, the number of heavy-heavy plasmids per cell
    :param dup: array of ints, the number of plasmids to duplicate
    :return: tuple(array of ints), new ll, lh, hh vectors

    """

    totn = (ll + lh + hh).astype(np.float32)
    llw = ll/totn
    lhw = lh/totn
    hhw = hh/totn
    add = [np.random.multinomial(dupi, [llwi, lhwi, hhwi]) for dupi, llwi, lhwi, hhwi in zip(dup, llw, lhw, hhw)]

    return tuple(np.array(add).T)

def comparison_with_molecules_per_cell(plasmids_per_time, plot=''):
    """
    Compare the number of plasmids across time with real data.
    """

    hours = [0, 9, 11, 20, 33, 36, 40, 60]
    ebv_plasmids_per_cell = [25.4, 20.3, 121.7, 2593., 7249., 7793., 7968., 8398.]

    if len(plot) > 0:
        import matplotlib.pyplot as plt
        plt.plot(hours, ebv_plasmids_per_cell)
        plt.plot(plasmids_per_time[0], plasmids_per_time[1])
        plt.xlabel('TIME (hrs)')
        plt.ylabel('PLASMIDS PER CELL')
        plt.title(plot[:-4])
        plt.savefig(plot, transparency=True)
        plt.clf()

    return distance_metric(np.array([hours, ebv_plasmids_per_cell]),
                           np.array(plasmids_per_time), True)


def simulate_meselson_stahl(ll, lh, hh, ll_dup, lh_dup, hh_dup, ts1, ts2, replication_1_time,
                            replication_2_time, brdu, measurement):
    """
    Simulate a Meselson Stahl experiment using examples taken from Thejas'
    data. Uses fractions rather than densities given that fractions have been
    normalized by density and it otherwise does not matter.
    """

    # First, we have to find the center point (by corrected fraction)
    # for each of the three weights, LL, LH, HH.
    # We are fitting only to the first replicate, for speed
    # We are also skipping 90 mins at 8 hours because the fits suggest
    # that these data do not match the rest
    data = {
        '8-1': (22.16, 17.20, 13.00),
        '8-1.5': (21.93, 16.42, 11.03),
        '8-2': (21.69, 15.96, 10.89),
        '8-3': (21.52, 15.91, 10.78),
        '30-3': (21.45, 16.18, 13.00),
        '30-6': (21.66, 16.20, 11.25),
    }

    # Next, we use the standard deviation fit to 10kb fragments of 4012
    # as en estimate of the minimum amount of noise.
    noise = 0.578

    # Get the partials
    np.warnings.filterwarnings('ignore')
    h = brdu + measurement
    frac_ts_1 = np.clip((ts1 - h)/replication_1_time, 0, 1)
    frac_pos_1 = np.isfinite(frac_ts_1)
    frac_ts_2 = np.clip((ts2 - h)/replication_2_time, 0, 1)
    frac_pos_2 = np.isfinite(frac_ts_2)

    # Get the simple parts of the distributions
    c_ll, c_lh, c_hh = data['%i-%i' % (brdu, measurement)]
    dist = [np.random.normal(c_ll, noise, np.sum(ll)),
            np.random.normal(c_lh, noise, np.sum(lh)),
            np.random.normal(c_hh, noise, np.sum(hh)
                             + 2*np.sum(hh_dup[np.bitwise_or(frac_pos_1, frac_pos_2)])
                             + np.sum(lh_dup[np.bitwise_or(frac_pos_1, frac_pos_2)]))]

    # Calculate the centers of the mixed distributions
    extra_ll_1 = c_ll - (c_ll - c_lh)*frac_ts_1[frac_pos_1]
    extra_ll_2 = c_ll - (c_ll - c_lh)*frac_ts_2[frac_pos_2]
    extra_lh_1 = c_lh - (c_lh - c_hh)*frac_ts_1[frac_pos_1]
    extra_lh_2 = c_lh - (c_lh - c_hh)*frac_ts_2[frac_pos_2]

    # Expand to an array by the number of duplicated plasmids
    if len(extra_ll_1) > 0:
        ll_dup_dist_1 = np.random.normal(np.concatenate([[c]*n for c, n in zip(extra_ll_1, ll_dup[frac_pos_1])]), noise)
        dist.append(ll_dup_dist_1)
        dist.append(ll_dup_dist_1)
    
    if len(extra_ll_2) > 0:
        ll_dup_dist_2 = np.random.normal(np.concatenate([[c]*n for c, n in zip(extra_ll_2, ll_dup[frac_pos_2])]), noise)
        dist.append(ll_dup_dist_2)
        dist.append(ll_dup_dist_2)
    
    if len(extra_lh_1) > 0:
        lh_dup_dist_1 = np.random.normal(np.concatenate([[c]*n for c, n in zip(extra_lh_1, ll_dup[frac_pos_1])]), noise)
        dist.append(lh_dup_dist_1)
    
    if len(extra_lh_2) > 0:
        lh_dup_dist_2 = np.random.normal(np.concatenate([[c]*n for c, n in zip(extra_lh_2, ll_dup[frac_pos_2])]), noise)
        dist.append(lh_dup_dist_2)

    frac = np.linspace(5, 29, 25)
    hist, _ = np.histogram(np.concatenate(dist), frac-0.5)
    return frac, hist


def gaussian(x, mu, sigma):
    constant = (1.0/(np.sqrt(2*np.pi*sigma*sigma)))
    exponent = np.exp(-(x - mu)*(x - mu)/(2*sigma*sigma))
    return constant*exponent


def gmm(x, all_sigma, mu1, peak1, mu2, peak2, mu3, peak3):
    """
    Gaussian mixture model
    """

    return (peak1*gaussian(x, mu1, all_sigma)
            + peak2*gaussian(x, mu2, all_sigma)
            + peak3*gaussian(x, mu3, all_sigma))


def compare_meselson_stahl(frac, dist, brdu, measure, plot='', compare_fits=True):
    """
    Compare simulated data with measured data and return the difference.
    """
    
    data = {
        '8-1': [[8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27], [844.47, 2214.21, 3960.92, 3924.97, 4689.50, 6863.22, 14265.69, 25799.09, 34630.48, 27743.75, 26490.77, 42332.79, 70007.72, 102401.75, 162686.61, 177526.40, 33187.11, 7986.10, 7230.79, 2682.22]],
        '8-1.5': [[8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25], [1150.88, 3738.22, 17314.64, 28688.15, 17325.58, 9981.73, 11138.27, 26485.99, 61492.02, 53464.31, 20181.00, 12415.73, 21239.12, 109795.79, 315974.48, 77422.79, 7230.79, 2682.22]],
        '8-2': [[7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25], [5044.82, 21763.25, 71408.15, 184058.30, 188861.87, 142396.14, 116736.07, 186025.57, 377940.40, 505445.99, 362213.63, 163697.29, 156657.13, 220013.07, 531438.44, 732453.57, 311390.22, 81046.24, 38876.82]],
        '8-3': [[5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26], [3072.86, 9715.15, 21894.38, 66335.48, 225840.64, 514757.30, 564175.07, 381956.06, 239463.72, 310301.14, 527442.23, 639341.87, 491489.20, 242935.93, 147446.56, 219924.67, 462597.95, 485053.54, 224142.42, 77874.32, 31842.81, 37484.11]],
        '30-3': [[10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27], [2506582.6, 2860672.1, 2811531.6, 3354226.4, 6856511.3, 16084985.3, 22955910.0, 16141119.7, 12168979.0, 13781850.0, 25462148.0, 82309493.3, 79128354.7, 21276524.7, 7265192.2, 2924797.0, 1895848.7, 1343359.1]],
        '30-6': [[7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28], [1647909.1, 2262821.1, 5418460.6, 9108792.3, 10849453.7, 9060058.3, 9531807.7, 13840482.3, 34886262.7, 50565613.3, 39777444.0, 24271789.3, 19372575.3, 32535914.7, 80648234.7, 109008330.7, 40751092.0, 12142723.7, 4761774.8, 5154736.5, 4334406.8, 3744852.2]],
    }

    fitdata = {# sigma, mu1-LL, peak1-LL, mu2-LH, peak2-LH, mu3-HH, peak3-HH
        '8-1': (22.16, 17.20, 13.00, 0.776, 0.174, 0.050),
        '8-1.5': (21.93, 16.42, 11.03, 0.737, 0.186, 0.078),
        '8-2': (21.69, 15.96, 10.89, 0.476, 0.371, 0.153),
        '8-3': (21.52, 15.91, 10.78, 0.279, 0.385, 0.336),
        '30-3': (21.45, 16.18, 13.00, 0.749, 0.210, 0.041),
        '30-6': (21.66, 16.20, 11.25, 0.601, 0.322, 0.077),
    }

    if compare_fits:
        mu1, mu2, mu3, area1, area2, area3 = fitdata['%i-%i' % (brdu, measure)]

        fixedgmm = lambda x, s, p1, p2, p3: gmm(x, s, mu1, p1, mu2, p2, mu3, p3)
        popt, _ = curve_fit(fixedgmm, frac[:-1]+0.5, dist.astype(np.float32)/np.max(dist), p0=[1, 0.5, 0.5, 0.1],  # sigma, peak1-LL, peak2-LH, peak3-HH
                            bounds=([-np.inf, 0, 0, 0], [np.inf, np.inf, np.inf, np.inf]))

        return distance_metric(np.array([area1, area2, area3]), popt[1:4]/np.sum(popt[1:4]))
    else:
        # Compare complete data
        real_frac, real_dist = data['%i-%i' % (brdu, measure)]
        real_dist = np.array(real_dist)/np.max(real_dist)
        test_dist = dist.astype(np.float32)/np.max(dist)

        if len(plot) > 0:
            import matplotlib.pyplot as plt
            plt.plot(real_frac, real_dist)
            plt.plot(frac[:-1], test_dist)
            plt.xlabel('FRACTION')
            plt.ylabel('COPIES')
            plt.title(plot[:-4])
            plt.savefig(plot, transparency=True)
            plt.clf()

        return distance_metric(np.array([real_frac, real_dist]), np.array([frac[:-1], test_dist]))


def search(maximal_synthesis=(100, 200, 500, 1000, 2000, 5000, 10000),
           saturation_constant=(100, 200, 500, 1000, 2000, 5000, 10000),
           polymerase_turnover_slope=(0.001, 0.002, 0.005, 0.008, 0.01, 0.02, 0.05),
           polymerase_turnover_offset=(100, 200, 500, 1000, 2000, 10000),
           replication_1_time=(0.6, 1.0, 1.5, 2.0, 3.0, 4.0),
           replication_1_duration=(999,),  # (1, 2, 4, 6, 8, 10, 15, 20),
           replication_2_time=(999,),  # (0.6, 1.0, 1.5, 2.0, 3.0, 4.0, 6.0, 8.0, 10.0),
           duplication_rate=(0.9,),
           cell_initialization_mu=25,
           cell_initialization_sigma=5,
           ncells=1000,
           dt_hours=None,
           noise_in_fractionation=0.5):
    """
    Search over variable space to find the optimal parameters
    """

    brdumeasure = [(8, 1), (8, 2), (8, 3), (30, 3), (30, 6)]

    dt = dt_hours
    for m in maximal_synthesis:
        for s in saturation_constant:
            if m <= s:
                for ps in polymerase_turnover_slope:
                    for po in polymerase_turnover_offset:
                        for r1 in replication_1_time:
                            for rd in replication_1_duration:
                                for r2 in replication_2_time:
                                    if r1 <= r2:
                                        for d in duplication_rate:
                                            if dt_hours is None:
                                                dt = r1/8.0
                                            print m, s, ps, po, r1, rd, r2, d,

                                            chisquares = []

                                            for brdu, measure in brdumeasure:
                                                try:
                                                    ll, lh, hh, ll_dup, lh_dup, hh_dup, ts1, ts2, plasmids_per_time = \
                                                        simulate(m, s, ps, po, r1, rd, r2, d, brdu, measure,
                                                                 dt, cell_initialization_mu,
                                                                 cell_initialization_sigma,
                                                                 ncells)
                                                    frac, dist = simulate_meselson_stahl(ll, lh, hh, ll_dup, lh_dup, hh_dup,
                                                                                         ts1, ts2, r1, r2, brdu, measure)
                                                    chisq = compare_meselson_stahl(frac, dist, brdu, measure)

                                                    chisquares.append(chisq)
                                                    print chisq,
                                                except ValueError:
                                                    chisquares.append(-1)
                                                    print -1,

                                            try:
                                                ll, lh, hh, ll_dup, lh_dup, hh_dup, ts1, ts2, plasmids_per_time = \
                                                    simulate(m, s, ps, po, r1, rd, r2, d, 61, 0,
                                                             dt, cell_initialization_mu,
                                                             cell_initialization_sigma,
                                                             ncells)
                                                chisq = comparison_with_molecules_per_cell(plasmids_per_time)
                                                chisquares.append(np.log(chisq)/5)
                                                print chisq,
                                            except ValueError:
                                                chisquares.append(-1)
                                                print -1,

                                            if len(chisquares) > 0 and np.nanmin(chisquares) > 0:
                                                print np.nanmean(chisquares)
                                            else:
                                                print 'failed'


def plot_simulation(maximal_synthesis=5000, # 200,
                    saturation_constant=10000, # 500,
                    polymerase_turnover_slope=0.05,
                    polymerase_turnover_offset=10000,
                    replication_1_time=0.6,  # 0.6 is the absolute minimum
                    replication_1_duration=999,
                    replication_2_time=999,
                    duplication_rate=0.9,
                    cell_initialization_mu=25,
                    cell_initialization_sigma=5,
                    ncells=10000,
                    dt_hours=None,
                    noise_in_fractionation=0.5):
    """
    Search over variable space to find the optimal parameters
    """

    np.set_printoptions(precision=2, suppress=True)
    # brdumeasure = [(8, 1), (8, 1.5), (8, 2), (8, 3), (30, 3), (30, 6)]
    brdumeasure = [(8, 1.5), (8, 3)]

    if maximal_synthesis > saturation_constant:
        raise ValueError('Maximal synthesis must be less than or equal to the saturation constant')

    dt = dt_hours
    if dt_hours is None:
        dt = replication_1_time/8.0

    for brdu, measure in brdumeasure:
        ll, lh, hh, ll_dup, lh_dup, hh_dup, ts1, ts2, plasmids_per_time = \
            simulate(maximal_synthesis, saturation_constant, polymerase_turnover_slope, polymerase_turnover_offset, replication_1_time, 
                     replication_1_duration, replication_2_time, duplication_rate, brdu, measure,
                     dt, cell_initialization_mu,
                     cell_initialization_sigma,
                     ncells)
        frac, dist = simulate_meselson_stahl(ll, lh, hh, ll_dup, lh_dup, hh_dup,
                                             ts1, ts2, replication_1_time, replication_2_time, brdu, measure)
        chisq = compare_meselson_stahl(frac, dist, brdu, measure, 'm%i s%i ps%f po%i r1 %.1f rd%i r2 %.1f h%i-brdu-%i.png' % 
            (maximal_synthesis, saturation_constant, polymerase_turnover_slope, polymerase_turnover_offset, replication_1_time, replication_1_duration, replication_2_time, brdu, measure))
        print chisq,

    ll, lh, hh, ll_dup, lh_dup, hh_dup, ts1, ts2, plasmids_per_time = \
        simulate(maximal_synthesis, saturation_constant, polymerase_turnover_slope, polymerase_turnover_offset, replication_1_time, 
                 replication_1_duration, replication_2_time, duplication_rate, 61, 0,
                 dt, cell_initialization_mu,
                 cell_initialization_sigma,
                 ncells)
    chisq = comparison_with_molecules_per_cell(plasmids_per_time, 'm%i s%i ps%f po%i r1 %.1f rd%i r2 %.1f h60 nplasmids.png' % 
            (maximal_synthesis, saturation_constant, polymerase_turnover_slope, polymerase_turnover_offset, replication_1_time, replication_1_duration, replication_2_time))
    print chisq


if __name__ == '__main__':
    search()
    # plot_simulation()
